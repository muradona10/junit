package org.md.junit.test;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.md.junit.model.Customer;
import org.md.junit.service.MoneyTransferService;

public class MoneyTransferServiceTest {
	
	public Customer sender;
	public Customer receiver;
	public MoneyTransferService moneyTransferService = new MoneyTransferService();
	
	/**
	 * This function works only once before all tests run
	 * @throws Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		System.out.println("Preparing the tests...");
	}
	
	/**
	 * This function works only once after all tests run
	 * @throws Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		System.out.println("All tests run...");
	}

	/**
	 * This function works before each test run
	 * @throws Exception
	 */
	@Before
	public void setUp() throws Exception {
		System.out.println("Preparing customers...");
		sender = new Customer(1, "Sender", 295, 6666000, 1250.00);
		receiver = new Customer(2, "receiver", 295, 6666001, 3250.00);
	}

	/**
	 * This function works after each test run
	 * @throws Exception
	 */
	@After
	public void tearDown() throws Exception {
		System.out.println("Clearing customers...");
		sender = null;
		receiver = null;
	}

	/**
	 * Test deposit function
	 */
	@Test
	public void testDeposit() {
		moneyTransferService.deposit(sender, 3000.00);
		//Control
		assertEquals("Test Fails: ", new Double(4250.00), sender.getBalanceAmnt());
	}
	
	/**
	 * Test withdraw function case: All money are withdrawer
	 */
	@Test
	public void testWithdrawAllMoney() {
		moneyTransferService.withdraw(sender, 1250.00);
		//Control
		assertEquals("Test Fails: ", new Double(0), sender.getBalanceAmnt());
	}
	
	/**
	 * Test withdraw function case: Balance is available 
	 */
	@Test
	public void testWithdrawBalanceIsOk() {
		moneyTransferService.withdraw(sender, 550.00);
		//Control
		assertEquals("Test Fails: ", new Double(700.00), sender.getBalanceAmnt());
	}
	
	/**
	 * Test withdraw function case: Balance is not available 
	 */
	@Test
	public void testWithdrawBalanceIsNok() {
		moneyTransferService.withdraw(sender, 1550.00);
		//Control
		assertEquals("Test Fails: ", new Double(1250.00), sender.getBalanceAmnt());
	}
	
	/**
	 * Test transferMoney function case: Balance is available 
	 */
	@Test
	public void testTransferMoneyBalanceIsOk() {
		moneyTransferService.transferMoney(sender, receiver, 800.00);
		//Control
		assertEquals("Test Fails: ", new Double(450.00), sender.getBalanceAmnt());
		assertEquals("Test Fails: ", new Double(4050.00), receiver.getBalanceAmnt());
	}
	
	/**
	 * Test transferMoney function case: Balance is not available 
	 */
	@Test
	public void testTransferMoneyBalanceIsNok() {
		moneyTransferService.transferMoney(sender, receiver, 1800.00);
		//Control
		assertEquals("Test Fails: ", new Double(1250.00), sender.getBalanceAmnt());
		assertEquals("Test Fails: ", new Double(3250.00), receiver.getBalanceAmnt());
	}
	
}
