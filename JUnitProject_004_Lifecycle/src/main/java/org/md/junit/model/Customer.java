package org.md.junit.model;

public class Customer {

	private Integer id;
	private String name;
	private Integer unitNum;
	private Integer accountNum;
	private Double balanceAmnt;
	
	public Customer() {}
	
	public Customer(Integer id, String name, Integer unitNum, Integer accountNum, Double balanceAmnt) {
		super();
		this.id = id;
		this.name = name;
		this.unitNum = unitNum;
		this.accountNum = accountNum;
		this.balanceAmnt = balanceAmnt;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getUnitNum() {
		return unitNum;
	}

	public void setUnitNum(Integer unitNum) {
		this.unitNum = unitNum;
	}

	public Integer getAccountNum() {
		return accountNum;
	}

	public void setAccountNum(Integer accountNum) {
		this.accountNum = accountNum;
	}

	public Double getBalanceAmnt() {
		return balanceAmnt;
	}

	public void setBalanceAmnt(Double balanceAmnt) {
		this.balanceAmnt = balanceAmnt;
	}
	
	
}
