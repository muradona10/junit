package org.md.junit.service;

import org.md.junit.model.Customer;

public class MoneyTransferService {
	
	public void deposit(Customer customer, Double money) {
		customer.setBalanceAmnt(customer.getBalanceAmnt()+money);
	}
	
	public void withdraw(Customer customer, Double money) {
		if(customer.getBalanceAmnt() >= money) {
			customer.setBalanceAmnt(customer.getBalanceAmnt() - money);
		}else {
			System.out.println("Balance is not enough!");
		}
	}
	
	public void transferMoney(Customer sender, Customer receiver, Double money) {
		if(sender.getBalanceAmnt()>=money) {
			sender.setBalanceAmnt(sender.getBalanceAmnt() - money);
			receiver.setBalanceAmnt(receiver.getBalanceAmnt() + money);
		}else {
			System.out.println("Sender balance is not enough!");
		}
	}

}
