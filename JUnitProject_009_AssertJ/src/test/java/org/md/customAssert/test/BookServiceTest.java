package org.md.customAssert.test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.md.customAssert.Assert.BookServiceAssert;
import com.md.customAssert.model.Book;
import com.md.customAssert.service.BookService;

public class BookServiceTest {
	
	private BookService bookService;

	@Before
	public void setUp() throws Exception {
		bookService  = new BookService();
	}

	@Test
	public void testWithJUnit() {
		Book book = new Book("Book1", 12.5);
		bookService.addBook(book);
		assertNotNull(bookService.getBooks());
		assertTrue(bookService.find("Book1"));
		assertEquals(1, bookService.getBooks().size());
	}
	
	/**
	 * Custom assertThat function 
	 * @param book
	 * @return
	 */
	private BookServiceAssert assertThatBook(BookService bookService) {
		return new BookServiceAssert(bookService,BookServiceAssert.class);
	}
	
	@Test
	public void testWithCustomAssert() {
		Book book = new Book("Book1", 12.5);
		bookService.addBook(book);
		
		/**
		 * We can add lots of function by using "." operator
		 * Every function must be added into the Custom Assert Class
		 */
		assertThatBook(bookService).findBook("Book1")
		                           .getSize(1);
		
		Book book2 = new Book("Book2", 32.5);
		Book book3 = new Book("Book3", 41.5);
		Book book4 = new Book("Book4", 8.80);
		bookService.addBook(book2);
		bookService.addBook(book3);
		bookService.addBook(book4);
		
		assertThatBook(bookService).findBook("Book3")
		                           .getSize(4);
	}

}
