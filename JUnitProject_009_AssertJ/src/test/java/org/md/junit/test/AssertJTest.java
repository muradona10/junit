package org.md.junit.test;

import org.assertj.core.api.Assertions;
import org.junit.Test;

public class AssertJTest {

	@Test
	public void testMatches() {

		String club = "Fenerbah�e";
	
		Assertions.assertThat(club)
		          .describedAs("Test Fail!")             // Test fail message
                  .isNotNull()                           // Not null control
		          .isNotEmpty()                          // Not empty control 
		          .startsWith("F")                       // Starts with control
		          .endsWith("e")                         // Ends with control
		          .contains("bah�e")                     // char sequence is in the string 
		          .doesNotContain("saray")               // char sequence is not in the string
		          .isEqualTo("Fenerbah�e")               // control all string
		          .isEqualToIgnoringCase("fenerBah�e")   // control all string by ignoring the case
		          .containsOnlyOnce("er");               // control only once 
		
	}

}
