package org.md.junit.test;

import java.io.File;

import org.assertj.core.api.Assertions;
import org.junit.Test;

public class AssertJFileTest {

	@Test
	public void test() {
		
		ClassLoader loader = new AssertJFileTest().getClass().getClassLoader();
		File file = new File(loader.getResource("Clubs.txt").getFile());
		
		Assertions.assertThat(file).exists()
		                           .canRead()
		                           .canWrite()
		                           .hasExtension("txt")
		                           .hasName("Clubs.txt");
		
		
		Assertions.assertThat(Assertions.contentOf(file))
		                           .isNotEmpty()
		                           .startsWith("Fener")
		                           .hasLineCount(4);
		
	}

}
