package org.md.junit.test;

import java.util.ArrayList;
import java.util.List;

import org.assertj.core.api.Assertions;
import org.junit.Test;

public class AsserJExtractTest {
	
	static class Player {
		
		private int number;
		private String name;
		
		public Player(int number, String name) {
			super();
			this.number = number;
			this.name = name;
		}
		public int getNumber() {
			return number;
		}
		public void setNumber(int number) {
			this.number = number;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		
	}

	@Test
	public void testExraction() {
		
	    List<Player> players = new ArrayList<Player>();
		players.add(new Player(1, "David De Gea"));
		players.add(new Player(6, "Paul Pogba"));
		players.add(new Player(7, "Cristiano Ronaldo"));
		players.add(new Player(10, "Alex De Souza"));
		players.add(new Player(10, "Ronaldinho"));
		players.add(new Player(14, "Thierry Henry"));
		
		/**
		 * Control players list contains 7 and 10 numbers
		 */
		Assertions.assertThat(players).describedAs("Test Fail!")
		                              .extracting("number")
		                              .contains(7,10);
		
		/**
		 * Control players list contains Paul Pogba and Alex De Souza
		 */
		Assertions.assertThat(players).describedAs("Test Fail!")
                                      .extracting("name")
                                      .contains("Paul Pogba","Alex De Souza");
		
		/**
		 * Control players list contains 3 tuples which are
		 * (7,"Cristiano Ronaldo"),(10,"Alex De Souze"),(1,""David De Gea)
		 */
		Assertions.assertThat(players).describedAs("Test Fail!")
                                      .extracting("number","name")
                                      .contains(
                                    		 Assertions.tuple(7,"Cristiano Ronaldo"),
                                    		 Assertions.tuple(10,"Alex De Souza"),
                                    		 Assertions.tuple(1,"David De Gea")
                                       );
		
		/**
		 * Control players list contains 1,7,10 and 14 numbers
		 * 
		 * DIFFERENT STYLE  
		 */
		Assertions.assertThat(
				Assertions.extractProperty("number",Integer.class).from(players)
				).contains(1,7,10,14);
		
		
	}

}
