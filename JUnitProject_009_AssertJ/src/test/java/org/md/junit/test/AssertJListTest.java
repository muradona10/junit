package org.md.junit.test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.assertj.core.api.Assertions;
import org.assertj.core.api.Condition;
import org.junit.Test;

public class AssertJListTest {
	
	public List<String> turkishClubs = new ArrayList<String>((Arrays.asList("Fenerbahce","Galatasaray","Besiktas","Trabzonspor")));
	public List<String> spanishClubs = new ArrayList<String>((Arrays.asList("Real Madrid","Barcelona","Valencia","Sevilla")));

	@Test
	public void test() {
		Assertions.assertThat(turkishClubs)
		          .describedAs("Test Fails!")
		          .isNotNull()
		          .contains("Fenerbahce","Besiktas")
		          .doesNotContain("Bursaspor")
		          .containsExactly("Fenerbahce","Galatasaray","Besiktas","Trabzonspor")
		          .doesNotHaveDuplicates();
	}
	
	@Test
	public void testOnlyTurkishClubs() {
		List<String> testClubs = new ArrayList<String>((Arrays.asList("Fenerbahce","Besiktas","Trabzonspor")));
		
		Assertions.assertThat(testClubs).have(onlyTurkishClubs());
	}

	private Condition<? super String> onlyTurkishClubs() {
		return new Condition<String>() {
			@Override
			public boolean matches(String s) {
				return turkishClubs.contains(s);
			}
		};
	}
	
	@Test
	public void testOnlySpanishClubs() {
		List<String> testClubs = new ArrayList<String>((Arrays.asList("Valencia","Sevilla","Real Madrid")));
		
		Assertions.assertThat(testClubs).have(onlySpanishClubs());
	}

	private Condition<? super String> onlySpanishClubs() {
		return new Condition<String>() {
			@Override
			public boolean matches(String s) {
				return spanishClubs.contains(s);
			}
		};
		
	}
	
	
	@Test
	public void testTwoSpanishClubAndTwoTurkishClub() {
		List<String> testClubs = new ArrayList<String>((Arrays.asList("Galatasaray","Valencia","Fenerbahce","Real Madrid")));
		
		Assertions.assertThat(testClubs).haveExactly(2, onlyTurkishClubs())
		                                .haveExactly(2, onlySpanishClubs());
	}

	

}
