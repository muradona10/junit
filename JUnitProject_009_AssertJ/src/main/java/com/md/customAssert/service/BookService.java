package com.md.customAssert.service;

import java.util.ArrayList;
import java.util.List;

import com.md.customAssert.model.Book;

public class BookService {

	private List<Book> books = new ArrayList<Book>();
	
	public void addBook(Book b) {
		books.add(b);
	}
	
	public Boolean find(String name) {
		for(Book b : books) {
			if(b.getName().equals(name)) return true;
		}
		return false;
	}

	public List<Book> getBooks() {
		return books;
	}
	
	
	
}
