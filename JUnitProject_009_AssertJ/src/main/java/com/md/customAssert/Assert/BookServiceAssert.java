package com.md.customAssert.Assert;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.assertj.core.api.AbstractAssert;

import com.md.customAssert.model.Book;
import com.md.customAssert.service.BookService;

public class BookServiceAssert extends AbstractAssert<BookServiceAssert, BookService>{

	public BookServiceAssert(BookService actual, Class<?> selfType) {
		super(actual, selfType);
		isNotNull();
		isNotIn(actual.getBooks());
	}
	
	public BookServiceAssert findBook(String name) {
		assertTrue(actual.find(name));
		return this;
	}
	
	public BookServiceAssert getSize(int i) {
		assertEquals(i, actual.getBooks().size());
		return this;
	}

}
