package org.md.junit.test;

import org.md.junit.model.MessageUtil;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class TestJunit {
	
	public MessageUtil messageUtil = new MessageUtil("Robert");
	
	
	@Before
	public void beforeEachTests() {
		System.out.println("*** BEFORE EACH TEST ***");
	}
	
	@BeforeClass
	public static void beforeClassOnce() {
		System.out.println("*** BEFORE ONLY ONCE ***");
	}
	
	@Test(expected = ArithmeticException.class)
	public void testPrintMessage() {
		System.out.println("Inside testPrintMessage()");
		messageUtil.printMessage();
		int a = 0;
		int b = 1 / a;
	}
	
	@Test
	public void testSaltationMessage() {
		System.out.println("Inside testSalutationMessage()");
		String message = "Hi! Robert";
		Assert.assertEquals(message, messageUtil.salutationMessage());
	}
	
	@AfterClass
	public static void afterClassOnce() {
		System.out.println("*** AFTER ONLY ONCE ***");
	}
	
	@After
	public void afterEachTests() {
		System.out.println("*** AFTER EACH TEST ***");
	}
	

}
