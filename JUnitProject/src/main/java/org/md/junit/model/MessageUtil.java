package org.md.junit.model;

public class MessageUtil {
	
	private String message;
	
	public MessageUtil(String message) {
		this.message = message;
	}
	
	public void printMessage() {
		System.out.println(message);
	}
	
	public String salutationMessage() {
		return "Hi! " + message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	
	

}
