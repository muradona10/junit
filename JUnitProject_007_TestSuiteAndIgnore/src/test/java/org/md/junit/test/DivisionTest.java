package org.md.junit.test;

import static org.junit.Assert.*;

import org.junit.Test;
import org.md.junit.model.Calculator;

public class DivisionTest {

	@Test
	public void testDivide() {
		
		Calculator calculator = new Calculator();
		
		assertEquals("Test Fails!", new Double(6.0), calculator.divide(54, 9));
		
	}

}
