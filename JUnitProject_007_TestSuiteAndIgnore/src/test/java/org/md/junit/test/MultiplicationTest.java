package org.md.junit.test;

import static org.junit.Assert.*;

import org.junit.Ignore;
import org.junit.Test;
import org.md.junit.model.Calculator;

public class MultiplicationTest {

	@Test
	public void testMultiply() {
		
		Calculator calculator = new Calculator();
		
		assertEquals("Test Fails!", 132, calculator.multiply(12, 11));
		
	}
	
	@Test
	public void testMultiplyByZero() {
		
		Calculator calculator = new Calculator();
		
		assertEquals("Test Fails!", 0, calculator.multiply(0, 11));
		
	}
	
	@Test
	@Ignore("This test has not finished yet")
	public void testMultiplyByOne() {
		
		Calculator calculator = new Calculator();
		
		assertEquals("Test Fails!", 11, calculator.multiply(1, 11));
		
	}
	

}
