package org.md.junit.test;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.md.junit.service.PisagorService;

@RunWith(Parameterized.class)
public class PisagorServiceTest {
	
	private PisagorService pisagorService = new PisagorService();
	
	private int a;
	private int b;
	private double hypotenuse;
	
	//Need constructor with all tested values
	public PisagorServiceTest(int a, int b, double hypotenuse) {
		this.a = a;
		this.b = b;
		this.hypotenuse = hypotenuse;
	}
	
	
	//Adjust Parameters
	@Parameters
	public static Collection<Object[]> parameters(){
		Object[][] data = {{3,4,5.0},{6,8,10.0},{8,15,17.0}};
		return Arrays.asList(data);
	}
	
	/**
	 * 0.01 is DELTA
	 */
	@Test
	public void testPisagor() {
		assertEquals(hypotenuse, pisagorService.pisagor(a, b),0.01);
	}


	public int getA() {
		return a;
	}


	public void setA(int a) {
		this.a = a;
	}


	public int getB() {
		return b;
	}


	public void setB(int b) {
		this.b = b;
	}


	public double getHypotenuse() {
		return hypotenuse;
	}


	public void setHypotenuse(double hypotenuse) {
		this.hypotenuse = hypotenuse;
	}
	
	


	

}
