package org.md.junit.test;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.md.junit.service.PisagorService;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(JUnitParamsRunner.class)
public class PisagorService2Test {

	private PisagorService pisagorService = new PisagorService();
	
	/**
	 * 0.01 is DELTA
	 */
	@Test
	@Parameters({"3,4,5.0","6,8,10.0","8,15,17.0"})
	public void testPisagor(int a, int b, double hypotenuse) {
		assertEquals(hypotenuse, pisagorService.pisagor(a, b),0.01);
	}

}
