package org.md.junit.model;

public class Calculator {
	
	public double divide(int a, int b) {
		if(b == 0) throw new ArithmeticException("Divide by zero exception occured!");
		return a/b;
	}

}
