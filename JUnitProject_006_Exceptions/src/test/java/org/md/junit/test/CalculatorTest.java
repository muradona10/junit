package org.md.junit.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.md.junit.model.Calculator;

import com.googlecode.catchexception.CatchException;

public class CalculatorTest {
	
	private Calculator calculator;
	
	@Before
	public void setUp() {
		calculator = new Calculator();
	}
	
	public void tearDown() {
		calculator = null;
	}

	@Test
	public void testDivideNoException() {
		
		int a = 12;
		int b = 4;
		
		// Calling Method
		double result = calculator.divide(a, b);
		
		// Controlling Results
		assertEquals("Test fails: ",3.0, result, 0.01);
		
	}
	
	/**
	 * Test exception case, Aspect 1: try-catch block
	 * We can control both exception and message
	 */
	@Test
	public void testDivideByZeroTryCatch() {
		
		int a = 8;
		int b = 0;
		
		try {
			calculator.divide(a, b);
		} catch (Exception ex) {
			assertTrue(ex instanceof ArithmeticException);
			assertEquals("Divide by zero exception occured!", ex.getMessage());
		}
		
	}
	
	/**
	 * Test exception case, Aspect 2: Test annotation parameter
	 * We can control both exception and message
	 */
	@Test(expected=ArithmeticException.class)
	public void testDivideByZeroWithTestParameterTrueException() {
		int a = 8;
		int b = 0;
		calculator.divide(a, b);
	}
	
	/**
	 * Test exception case, Aspect 2: Test annotation parameter, but wrong exception
	 * We can control both exception and message
	 */
	@Ignore
	@Test(expected=NullPointerException.class)
	public void testDivideByZeroWithTestParameterWrongException() {
		int a = 8;
		int b = 0;
		calculator.divide(a, b);
	}
	
	/**
	 * Test exception case Aspect 3: Using rule
	 * We define a rule and we use this rule in a test case
	 * We identify which exception occurs and which errorMessage displays
	 */
	@Rule
	public ExpectedException expectedException = ExpectedException.none();
	
	@Test
	public void testDivideByZeroWithRuleAnnotation() {
		
		// identify expectations
		expectedException.expect(ArithmeticException.class);
		expectedException.expectMessage("Divide by zero exception occured!");
		
		// call method
		calculator.divide(12, 0);
		
		// Junit compare expected results automatically
	}
	
	/**
	 * Test exception case Aspect 4: using catch-exception API
	 * add catch-exception api into the pom.xml
	 */
	@Test
	public void testDivideByZeroWithCatchExceptionAPI() {
		CatchException.catchException(calculator).divide(5, 0);
		
		assertTrue(CatchException.caughtException() instanceof ArithmeticException);
		assertEquals("Divide by zero exception occured!", CatchException.caughtException().getMessage());
	}
	
}
