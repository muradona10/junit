package org.md.mock.test;

import org.junit.Test;

import static org.mockito.Mockito.*;

import static org.assertj.core.api.Assertions.*;

import com.md.mock.service.PlayerService;

public class PlayerBehaviorTest {

	@Test
	public void testWhenThenReturn() {
		
		// Mock PlayerService
		PlayerService playerService = mock(PlayerService.class);
		
		// If we sent "Alex" as a parameter to the returnPlayer then 
		// this function returns "Player Alex" text
		when(playerService.returnPlayer(eq("Alex"))).thenReturn("Player Alex");
		
		String playerName = playerService.returnPlayer("Alex");
		
		assertThat(playerName).isEqualTo("Player Alex");
		
		// If we sent any String parameter to the returnPlayer then
		// this function returns "Default Player"
		when(playerService.returnPlayer(anyString())).thenReturn("Default Player");
		
		String playerName2 = playerService.returnPlayer("Valbuena");
		
		assertThat(playerName2).isNotNull()
		                       .isNotEmpty()
		                       .startsWith("Default")
		                       .endsWith("er")
		                       .isEqualTo("Default Player");
	}
	
	@Test
	public void testWhenThenThrow() throws Exception {
		
		// Mock PlayerService
		PlayerService playerService = mock(PlayerService.class);
		
		// If we sent "Alex" as a parameter to the returnPlayer then 
		// this function throws an RuntimeException
		when(playerService.returnPlayer(eq("Alex"))).thenThrow(new RuntimeException("Runtime Exception Occured!"));
		
		String playerName = playerService.returnPlayer("Alex");
		
		/*
		assertThat(playerName).isNotNull()
		                       .isNotEmpty()
		                       .startsWith("Default")
		                       .endsWith("er")
		                       .isEqualTo("Default Player");
		*/
	}
	
	@Test
	public void testDo() throws Exception {
		
		// Mock PlayerService
		PlayerService playerService = mock(PlayerService.class);
		
		// for void returned functions
		doNothing().when(playerService).addPlayer(anyString());
		doThrow(new RuntimeException("Runtime Exception Occured!")).when(playerService).removePlayer(anyString());
		
		// for functions that has returnedValues
		doReturn("Alex").when(playerService).returnPlayer(eq("Alex"));
		String returnedString = playerService.returnPlayer("Alex");
		assertThat(returnedString).isNotEmpty().isNotNull().isEqualTo("Alex");
	
	}
	
	

}
