package org.md.mock.test;

import static org.junit.Assert.*;

import org.junit.Test;
import static org.mockito.Mockito.*;

import static org.assertj.core.api.Assertions.*;

import com.md.mock.service.PlayerService;
import com.md.mock.service.impl.PlayerServiceImpl;

public class PlayerServiceSPYTest {

	/**
	 * mock is unreal object, spy is used both real and unreal object
	 * to use spy real object is necessary unlike mock
	 */
	
	@Test
	public void testMockAndSpy() {
		//mock
		PlayerService mockService = mock(PlayerService.class);
		//spy
		PlayerService spyService = spy(new PlayerServiceImpl());
		
		//mock dont call real functions
		mockService.addPlayer("Alex"); // Not call real object
		spyService.addPlayer("Bolic"); // Default call real object
		
		doNothing().when(mockService).addPlayer("Ali");
		doReturn("Ahmet").when(mockService).returnPlayer("Selim");
		mockService.addPlayer("Ali");
		assertThat(mockService.returnPlayer("Selim")).isNotNull().isNotEmpty().isEqualTo("Ahmet");
		
		// spy use real or unreal objects we desire
		/**
		 * do not use real object when we call addPlayer with any string paramater
		 */
		doNothing().when(spyService).addPlayer(anyString()); 
		spyService.addPlayer("Pele"); // Not call real object, no write on the console
		
		/**
		 * use real object when we call addPlayer with "Maradona"
		 */
		doCallRealMethod().when(spyService).addPlayer(eq("Maradona")); 
		spyService.addPlayer("Maradona"); // call real object, write on the console
		
		/**
		 * default spy use real object
		 */
		spyService.removePlayer("Zidane");
		assertThat(spyService.returnPlayer("Ronaldo")).isEqualTo("Ronaldo");
		
		/*
		doThrow(new RuntimeException("Runtime Exception")).when(spyService).removePlayer(eq("Carlos"));
		spyService.removePlayer("Carlos");
		*/
	}
	
}
