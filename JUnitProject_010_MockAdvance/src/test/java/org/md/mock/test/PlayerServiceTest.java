package org.md.mock.test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;
import org.mockito.Mockito;

import com.md.mock.service.PlayerService;

public class PlayerServiceTest {

	private PlayerService playerService;
	
	@Before
	public void setUp() throws Exception {
		
		playerService = Mockito.mock(PlayerService.class);
		
		playerService.addPlayer("Valbuena");
		playerService.addPlayer("Joseph");
		playerService.addPlayer("Soldado");
		playerService.addPlayer("Gomis");
		playerService.addPlayer("Valbuena");
		playerService.addPlayer("Valbuena");
		playerService.addPlayer("Gomis");
	}

	/**
	 * Test a function is called with wanted parameter
	 */
	@Test
	public void testPlayerAddRemove() {
		
		Mockito.verify(playerService).addPlayer("Soldado");
		Mockito.verify(playerService).addPlayer("Joseph");
		// Mockito.verify(playerService).addPlayer("Hasan Ali"); // not called with this parameter
		
	}

	/**
	 * Test a function is called with wanted parameter, control times
	 */
	@Test
	public void testPlayerCount() {
		
		Mockito.verify(playerService,Mockito.times(3)).addPlayer("Valbuena");
		Mockito.verify(playerService,Mockito.times(2)).addPlayer("Gomis");
		Mockito.verify(playerService,Mockito.times(1)).addPlayer("Soldado");
		
		Mockito.verify(playerService,Mockito.never()).removePlayer(Mockito.anyString());
		
		Mockito.verify(playerService,Mockito.atMost(2)).addPlayer("Gomis");
		Mockito.verify(playerService,Mockito.atLeast(2)).addPlayer("Valbuena");
		
	}
	
	/**
	 * Control the order of calling functions
	 * change order to see test finish with error
	 * verifyNoMoreInteractions
	 * control no more calling occurs after all functions we tested
	 * uncomment playerService2.addPlayer("Player4") to see test fails
	 * verifyZeroInteractions
	 * control this mock has never used
	 * uncomment //playerService3.addPlayer("Player5"); to see test fails
	 */
	@Test
	public void testFunctionOrder() {
		
		PlayerService playerService2 = Mockito.mock(PlayerService.class);
		playerService2.addPlayer("Player1");
		playerService2.addPlayer("Player2");
		playerService2.removePlayer("Player2");
		playerService2.addPlayer("Player3");
		//playerService2.addPlayer("Player4");
		
		InOrder order = Mockito.inOrder(playerService2);
		order.verify(playerService2).addPlayer("Player1");
		order.verify(playerService2).addPlayer("Player2");
		order.verify(playerService2).removePlayer("Player2");
		order.verify(playerService2).addPlayer("Player3");
		
		Mockito.verifyNoMoreInteractions(playerService2); // Control No more calling occurs
		
		PlayerService playerService3 = Mockito.mock(PlayerService.class);
		//playerService3.addPlayer("Player5");
		Mockito.verifyZeroInteractions(playerService3);
		
	}
	
}
