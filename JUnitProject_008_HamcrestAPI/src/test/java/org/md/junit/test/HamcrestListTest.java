package org.md.junit.test;

import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.*;

import org.junit.Test;

public class HamcrestListTest {

	@Test
	public void testMatches() {

		List<String> clubs = new ArrayList<String>(Arrays.asList("Fenerbahce","Galatasaray","Besiktas","Trabzonspor"));
		
		/**
		 * Control list is not null
		 */
		assertThat(clubs, is(notNullValue()));
		
		/**
		 * Control list has an expected member
		 */
		assertThat(clubs, hasItem("Fenerbahce"));
		
		/**
		 * Control list has expected members
		 */
		assertThat(clubs, hasItems("Fenerbahce","Galatasaray"));
		
		/**
		 * Control logical OR for list
		 */
		assertThat(clubs, anyOf(hasItem("Besiktas"),hasItem("Tokatspor")));
		
		/**
		 * Control logical OR for list by different way
		 */
		assertThat(clubs, either(hasItem("Fenerbahce")).or(not(hasItem("Samsunspor"))));
		
		/**
		 * Control logical AND for list
		 */
		assertThat(clubs, allOf(hasItem("Fenerbahce"), hasItem("Trabzonspor")));
		
	}

}
