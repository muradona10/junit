package org.md.junit.test;

import static org.junit.Assert.assertThat;

import static org.hamcrest.CoreMatchers.*;

import org.junit.Test;

public class HamcrestTest {

	@Test
	public void testMatches() {

		String club = "Fenerbahce";
		String club2 = null;

		/**
		 * is equal to assertEquals(expected, actual)
		 */
		assertThat(club, is(equalTo("Fenerbahce")));

		/**
		 * Control a value is null
		 */
		assertThat(club2, is(nullValue()));

		/**
		 * Control a value is not null
		 */
		assertThat(club, is(notNullValue()));

		/**
		 * Control a string contains given substring
		 */
		assertThat(club, containsString("erbah"));

		/**
		 * Control logical OR
		 */
		assertThat(club, anyOf(containsString("Fener"), containsString("Basak")));

		/**
		 * Control logical AND
		 */
		assertThat(club, allOf(containsString("Fener"), containsString("bahce"), not(containsString("Basak"))));
	
		int number = 12;
		assertThat(number, is(equalTo(12)));
		assertThat(number, is(notNullValue()));
		
	}

}
