package org.md.junit.test;

import org.junit.Before;
import org.junit.Test;
import org.md.junit.dao.CustomerRepository;
import org.md.junit.model.Customer;
import org.md.junit.service.CustomerService;
import org.md.junit.service.NotificationService;
import org.mockito.Mockito;

public class CustomerServiceTest {
	
	private CustomerService customerService;
	private CustomerRepository customerRepository;
	private NotificationService notificationService;

	/**
	 * Before annotasyonu ile her test metodundan �nce cagrilacak
	 * Fonksiyon yazabiliriz boylece her test metodunun ihtiyac� olacak degiskenleri vs
	 * burada tan�mlayabiliriz.
	 * 
	 * Mockito kutuphanesi ile bir sinifin testi sirasinda ihtiyac duyacagimiz baska siniflar
	 * varsa onlari sahte nesne olarak kullanabiliriz, bizim icin istedi�gimiz bir siniftan
	 * bos ve sahte bir nesne olusturur. Mockito.mock()
	 * Mockito ile bir sinifin veya metodun testi yapilirken calismasi gereken metodlarini
	 * Mockito.verify() ile kontrol edebiliriz.
	 * @throws Exception
	 */
	@Before
	public void setUp() throws Exception {
		customerService = new CustomerService();
		customerRepository = Mockito.mock(CustomerRepository.class);
		notificationService = Mockito.mock(NotificationService.class);
		customerService.setCustomerRepository(customerRepository);
		customerService.setNotificationService(notificationService);
	}

	@Test
	public void testCustomerSave() {
		Customer customer = new Customer();
		customerService.customerSave(customer);
		
		// Mockito.verify ile beklenen method lar cagrilmis mi kontrol et
		Mockito.verify(customerRepository).saveCustomer(customer);
		Mockito.verify(notificationService).sentCustomerSavedMessage(customer);
	}

}
