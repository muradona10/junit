package org.md.junit.service;

import org.md.junit.model.Customer;

public class NotificationService {

	public void sentCustomerSavedMessage(Customer customer) {
		System.out.println("New customer saved." + customer.getName());
	}
	
	public void sentCustomerDeletedMessage(Customer customer) {
		System.out.println("Customer deleted." + customer.getName());
	}
	
}
