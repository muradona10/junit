package org.md.junit.service;

import org.md.junit.dao.CustomerRepository;
import org.md.junit.model.Customer;

public class CustomerService {

	private CustomerRepository customerRepository;
	private NotificationService notificationService;
	
	public void customerSave(Customer customer) {
		customerRepository.saveCustomer(customer);
		notificationService.sentCustomerSavedMessage(customer);
	}
	
	
	public CustomerRepository getCustomerRepository() {
		return customerRepository;
	}
	public void setCustomerRepository(CustomerRepository customerRepository) {
		this.customerRepository = customerRepository;
	}
	public NotificationService getNotificationService() {
		return notificationService;
	}
	public void setNotificationService(NotificationService notificationService) {
		this.notificationService = notificationService;
	}
	
	
	
}
