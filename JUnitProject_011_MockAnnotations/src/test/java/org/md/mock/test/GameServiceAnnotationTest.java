/**
 * 
 */
package org.md.mock.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.md.mock.service.RefreeService;
import com.md.mock.service.impl.GameService;
import com.md.mock.service.impl.PlayerServiceImpl;

/**
 * @author user
 *
 */
@RunWith(MockitoJUnitRunner.class) // Must for mockito annotations
public class GameServiceAnnotationTest {
	
	@InjectMocks  // inject dependencies, playerService and refreeService
	private GameService gameService; 
	
	@Spy          // make playerService spy
	private PlayerServiceImpl playerService;
	
	@Mock        // make refreeService mock
	private RefreeService refreeService;

	@Test
	public void testAnnotation() {
		
		gameService.game();
		
	}

}
