package com.md.mock.service.impl;

import com.md.mock.service.PlayerService;

public class PlayerServiceImpl implements PlayerService {

	// Mockito SPY
	
	public void addPlayer(String playerName) {
		System.out.println("Add Player    :" + playerName);
	} 

	public void removePlayer(String playerName) {
		System.out.println("Remove Player :" + playerName);
	}

	public String returnPlayer(String playerName) {
		System.out.println("Return Player :" + playerName);
		return playerName;
	}

}
