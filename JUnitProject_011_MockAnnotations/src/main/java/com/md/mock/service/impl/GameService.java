package com.md.mock.service.impl;

import com.md.mock.service.PlayerService;
import com.md.mock.service.RefreeService;

public class GameService {
	
	private PlayerService playerService;
	
	private RefreeService refreeService;
	
	public void game() {
		
		for(int i = 0 ; i < 11; i++) {
			String name = "Team A Player" + (i+1);
			playerService.addPlayer(name);
		}
		
		System.out.println("*************************************************************");
		
		for(int i = 0 ; i < 11; i++) {
			String name = "Team B Player" + (i+1);
			playerService.addPlayer(name);
		}
		
		refreeService.runGame("Refree");
		
		System.out.println("Game Started");
	}

	public PlayerService getPlayerService() {
		return playerService;
	}

	public void setPlayerService(PlayerService playerService) {
		this.playerService = playerService;
	}

	public RefreeService getRefreeService() {
		return refreeService;
	}

	public void setRefreeService(RefreeService refreeService) {
		this.refreeService = refreeService;
	}

}
