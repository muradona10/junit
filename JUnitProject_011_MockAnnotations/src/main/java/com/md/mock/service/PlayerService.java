package com.md.mock.service;

public interface PlayerService {
	
	public void addPlayer(String playerName);
	
	public void removePlayer(String playerName);
	
	// Mockito - identify behavior
	public String returnPlayer(String playerName);

}
