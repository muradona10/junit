package org.md.junit.test;

import static org.junit.Assert.assertEquals;

import org.junit.Ignore;
import org.junit.Test;
import org.md.junit.model.Calculator;

public class CalculatorTest {

	@Test
	public void testSum() {
		
		// Preparing
		Calculator calculator = new Calculator();
		int a = 12;
		int b = 7;
		
		// Calling Method
		int result = calculator.sum(a, b);
		
		// Controlling Results
		assertEquals(19, result);
		
	}
	
	@Test
	public void testSubtractFromZero() {
		Calculator calculator = new Calculator();
		assertEquals(-15, calculator.subtract(0, 15));
	}
	
	@Test
	public void testSubtractFromTwoPositiveNumberAndResultPositive() {
		Calculator calculator = new Calculator();
		assertEquals(10, calculator.subtract(97, 87));
	}
	
	/**
	 * Birim testinde sadece en kucuk yazilim parcasinin testi yapilmalidir
	 * Bir sinifin birden fazla metodunu ayni test case inde test edemeyiz
	 * Ornegin buradaki testte sum icin olan test hata alir ama altindaki subtract 
	 * icin olan test aslinda duzgudur ama biri hata aldigi icin tum unit test
	 * hata almis olacaktir. @Ignore kaldirilarak bu gorulebilir.
	 */
	@Ignore
	@Test
	public void testCalculatorAll() {
		Calculator calculator = new Calculator();
		
		//Test sum
		assertEquals(36, calculator.sum(11, 24));
		//Test subtract
		assertEquals(-8, calculator.subtract(12, 20));
	}
	
	/**
	 * Birim testinde sadece bir senaryo test edilmelidir
	 * Aksi halde hata alan bir senaryo yuzunden hata almayan seneryolar olmasina 
	 * ragmen tum test hata almis olur
	 */
	@Ignore
	@Test
	public void testSubtractTwoCase() {
		Calculator calculator = new Calculator();
		
		//Test subtract lower than higher
		assertEquals(-13, calculator.subtract(11, 24));
		//Test subtract same two value
		assertEquals(-8, calculator.subtract(20, 20));
		//Test subtract higher than lower
		assertEquals(22, calculator.subtract(23, 1));
	}
	

}
